package arrays1;

import java.util.Scanner;

public class Main {
    static Array1Task3 array = new Array1Task3();
    static Array3Task3 array2 = new Array3Task3();
    static Array4 game = new Array4();
    static  Array2Task3 array3 = new Array2Task3();
    public static  void main(String args[]){
        menu();
    }

    public static void task1(){
        int[] arr1 = array.getArray();
        int[] arr2 = array.getArray();
        int[] intersectionArray = array.getIntersectionArray(arr1, arr2);
        int[] uniqueArray = array.getUniqueArray(arr1, arr2);

        System.out.println("Our intersection array :" );
        for (int i = 0; i < intersectionArray.length; i++) {
            System.out.print(intersectionArray[i] + " ");
        }

        System.out.println("\nOur unique array :" );
        for (int i = 0; i < uniqueArray.length; i++) {
            System.out.print(uniqueArray[i] + " ");
            break;
        }
    }

    public static void menu(){
        System.out.println("Enter 1 to get task 1");
        System.out.println("Enter 2 to get task 2");
        System.out.println("Enter 3 to get task 3");
        System.out.println("Enter 4 to get task 4(game)");
        Scanner scan = new Scanner(System.in);
        int choose = scan.nextInt();
        switch (choose){
            case 1 :
                task1();
            case 2 :
               array2.delete();
            case 3:
               array3.getIntersectionArray() ;
            case 4 :
                game.chooseDoor();

        }
    }



}

