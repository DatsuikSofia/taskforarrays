package arrays1;

import java.util.*;


public class Array1Task3 {
    public int[] getArray() {
        System.out.println(" Enter size of array");
        Scanner scan = new Scanner(System.in);
        int size = scan.nextInt();
        int[] arr = new int[size];
        for(int i = 0; i < arr.length; i++) {
            arr[i] = 1 + (int) (Math.random() * 9);
            System.out.println("Our array : " + arr[i]);
        }

        return  arr;
    }

public static int[] CreateArray()  {
        Scanner scan = new Scanner(System.in);
        int size = scan.nextInt();
        int[] arr = new int[size];
        for (int count = 0; count < size; count++) {
        int number = (int) (Math.random() * (10) + 1);
        arr[count] = number;
        System.out.print(number + " ");
        }
        System.out.println();
        return arr;
        }

public static void AddUniqueValues() {
        int[] arr= CreateArray();
        int count = 0;
        int[] array3 = new int[0];

        Set<Integer> set = new HashSet<Integer>();
        for (int i = 0; i < arr.length; i++) {
        set.add(arr[i]);
        }
        Iterator<Integer> it = set.iterator();
        while (it.hasNext()) {
        array3 = Arrays.copyOf(array3, array3.length + 1);
        array3[count] = it.next();
        System.out.print(array3[count] + " ");
        }
        System.out.println();
        }

public static void OneArray()  {
        int[] array1 = CreateArray();
        int[] array2 = CreateArray();
        int size3 = array1.length > array2.length ? array2.length : array1.length;
        int count = 0;
        int[] array3 = new int[size3];
        for (int i = 0; i < array1.length; i++) {
        for (int j = 0; j < array2.length; j++) {
        if (array1[i] != array2[j]) {
        count++;
        }
        }
        if (count == array2.length) {

        array3[array3.length - 1] = array1[i];
        array3 = Arrays.copyOf(array3, array3.length + 1);
        System.out.print(array1[i] + " ");
        }
        count = 0;
        }
        System.out.println();
        }}




    /*public int[] getIntersectionArray(int[] arr1, int[] arr2) {
        int size3 = arr1.length > arr2.length ?  arr2.length :  arr1.length;
        int []array3 = new int[size3];
        int counter = 0;

        for( int i = 0 ; i < arr1.length ; i++) {
            for (int j = 0; j < arr2.length; j++) {
                if (arr1[i] == arr2[j]) {
                    array3[counter] = arr1[i];
                    arr2[j] = -1;
                    counter++;
                }
            }
        }
        int[] res = new int[counter];
        for (int i = 0; i < counter; i++) {
            res[i] = array3[i];
        }

        return res;
    }
    public int[] getUniqueArray(int[] arr1, int[] arr2) {
        int size3 = arr1.length + arr2.length;
        int []array3 = new int[size3];
        int counter = arr2.length;

        for( int i = 0 ; i < arr1.length ; i++) {
            array3[i] = arr1[i];
        }
        for(int i = 0; i < arr2.length; i++) {
            array3[counter] = arr2[i];
        }

        int[] res = new int[counter];
        for (int i = 0; i < counter; i++) {
            res[i] = array3[i];
        }

        return res;
    }
}*/
