package arrays1;

public class Array2Task3 {
    public void getIntersectionArray() {
        int[] arr1 ={1,2,5,6,7,8,9,};
        int[] arr2= {56,7,3,4,2,1,1,1,6,8,9,34,5,6,23};
        int size3 = arr1.length < arr2.length ?  arr2.length :  arr1.length;
        int []array3 = new int[size3];
        int counter = 0;

        for( int i = 0 ; i < arr1.length ; i++) {
            for (int j = 0; j < arr2.length; j++) {
                if (arr1[i] == arr2[j]) {
                    array3[counter] = arr1[i];
                    arr2[j] = -1;
                    counter++;
                }
            }
        }
        int[] res = new int[counter];
        for (int i = 0; i < counter; i++) {
            res[i] = array3[i];
        }

        System.out.println( res);
    }
}
